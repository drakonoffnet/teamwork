﻿namespace TeamWork.Infrastructure.Model
{
    public class CommentsDto
    {
        public int Id { get; set; }

        public int TaskId { get; set; }

        public int UserId { get; set; }

        public string Comment { get; set; }
    }
}