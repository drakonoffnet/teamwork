﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamWork.Controllers.Base;
using TeamWork.Infrastructure.Model;

namespace TeamWork.Controllers
{
    public class UserController : BaseController
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            var model = UserService.GetUsers();

            return View(model);
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id)
        {
            var model = UserService.GetUser(id);

            return View(model);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            ViewData["RoleList"] = RoleService.GetRoles();

            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(UserDto collection)
        {
            if (TryValidateModel(collection))
            {
                UserService.InsertUser(collection);

                return RedirectToAction("Index");
            }
            else
            {
                ViewData["RoleList"] = RoleService.GetRoles();
                return View(collection);
            }
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id)
        {
            var model = UserService.GetUser(id);

            return View(model);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(UserDto collection)
        {

            if (TryValidateModel(collection))
            {
                UserService.UpdateUser(collection);

                return RedirectToAction("Details", new {id = collection.Id});
            }
            else
            {
                return View();
            }
 
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(int id)
        {
            UserService.DeleteUser(id);

            return RedirectToAction("Index");
        }
 
    }
}
