﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamWork.Infrastructure.Model {
    public class UserDto 
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public bool IsActive { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }
    }
}
