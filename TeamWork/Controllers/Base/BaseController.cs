﻿using System.Web.Mvc;
using TeamWork.Core.Service;
using TeamWork.Infrastructure.Model;
using TeamWork.Infrastructure.Service;

namespace TeamWork.Controllers.Base
{
    public class BaseController : Controller
    {
        protected IProjectService ProjectService = new ProjectService();

        protected IUsers UserService = new UsersService();

        protected IRolesService RoleService = new RolesService();
    }
}