﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToDB;
using TeamWork.DAL.DataObjects;

namespace TeamWork.DAL 
{
    public class TeamWorkContext : LinqToDB.Data.DataConnection
    {
        public TeamWorkContext() : base("DefaultConnection") { }

        public ITable<User> Users { get { return GetTable<User>(); } }

        public ITable<Projects> Projects { get { return GetTable<Projects>(); } }

        public ITable<UserProjects> UserProjects { get { return GetTable<UserProjects>(); } }

        public ITable<Task> Tasks { get { return GetTable<Task>(); } }

        public ITable<Comments> Comments { get { return GetTable<Comments>(); } }

        public ITable<LogWork> LogWorks { get { return GetTable<LogWork>(); } }

        public ITable<Role> Roles { get { return GetTable<Role>(); } }

        public ITable<UsersInRole> UsersInRoles { get { return GetTable<UsersInRole>(); } }
        
    }
}
