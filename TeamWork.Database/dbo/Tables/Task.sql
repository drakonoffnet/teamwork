﻿CREATE TABLE [dbo].[Task](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateCreate] [datetime] NOT NULL,
	[DateUpdate] [datetime] NULL,
	[DateClouse] [datetime] NULL,
	[Name] [nchar](500) NULL,
	[Status] INT NOT NULL DEFAULT ((0)),
	[ShortDescription] [nchar](255) NOT NULL,
	[FullDescription] [nvarchar](max) NULL,
 [ReporterUserId] INT NOT NULL, 
    [IsActive] BIT NOT NULL DEFAULT ((0)), 
    CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY], 
    CONSTRAINT [FK_Task_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([Id]), 
    CONSTRAINT [FK_Task_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects]([Id]) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


CREATE INDEX [Assignee] ON [dbo].[Task] ([ProjectId], [UserId])
GO

CREATE INDEX [SelectTaskFromDate] ON [dbo].[Task] ([DateCreate], [UserId])
GO

CREATE INDEX [SelectTaskStatusFromUser] ON [dbo].[Task] ([UserId], [Status])
GO