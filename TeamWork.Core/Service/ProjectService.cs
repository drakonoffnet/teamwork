﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using LinqToDB;
using TeamWork.DAL.DataObjects;
using TeamWork.Infrastructure.Model;
using TeamWork.Infrastructure.Service;

namespace TeamWork.Core.Service
{
    public class ProjectService : BaseService, IProjectService
    {
        private TasksService _tasksService;

        public ProjectService()
        {
            Mapper.CreateMap<Projects, ProjectsDto>();
            Mapper.CreateMap<ProjectsDto, Projects>();

            _tasksService = new TasksService();
        }

        public IEnumerable<ProjectsDto> GetProjects(int take = 10, int skip = 0)
        {
            return Context.Projects.Take(take).Skip(skip)
                .Select(c => new ProjectsDto()
                {
                    Id = c.Id,
                    DateCreate = c.DateCreate,
                    CountTask = Context.Tasks.Count(t => t.ProjectId == c.Id),
                        Name = c.Name,
                        Tasks = new List<TaskDto>()
                    }).ToList();
         }

        public ProjectsDto GetProject(int projectId)
        {
            var project = Context.Projects.FirstOrDefault(c => c.Id == projectId);
            if (project != null)
            {
                var taskList = _tasksService.GetTaskForProject(project.Id);

                var result = Mapper.Map<ProjectsDto>(project);

                result.Tasks = taskList;

                result.CountTask = taskList.Count;

                return result;
            }

            return new ProjectsDto();
        }

        public ProjectsDto InsertProject(ProjectsDto item)
        {
            item.Id = Convert.ToInt32(Context.InsertWithIdentity(Mapper.Map<Projects>(item)));

            return item;
        }


        public bool UpdateProject(ProjectsDto item)
        {
            try
            {
                Context.Update(Mapper.Map<Projects>(item));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteProject(int projectId)
        {
            try
            {
                Context.Projects
                    .Where(p => p.Id == projectId)
                    .Set(p => p.IsActive, false)
                    .Update();

                return true;
            }
            catch (Exception)
            {
                return false;
                
            }
        }
    }
}
