﻿using System.Collections.Generic;
using TeamWork.Infrastructure.Model;

namespace TeamWork.Infrastructure.Service
{
    public interface ITasksService
    {
        List<TaskDto> GetTaskForProject(int projectId);

        List<TaskDto> GetAllTaskOneUser(int userId);

        TaskDto InserTask(TaskDto item);

        bool DeleteTask(int taskId);

        bool UpdateTask(TaskDto item);

        bool СhangeAssignee(int taskId, int newUserId);
    }
}