﻿using System;
using LinqToDB.Mapping;

namespace TeamWork.DAL.DataObjects
{
   [Table(Name = "Task")]
    public class Task
    {
       [Column(Name = "Id")]
       [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "ProjectId"), NotNull]
        public int ProjectId { get; set; }

        [Column(Name = "UserId"), NotNull]
        public int UserId { get; set; }

        [Column(Name = "ReporterUserId"), NotNull]
        public int ReporterUserId { get; set; }

        [Column(Name = "DateCreate"), NotNull]
        public DateTime DateCreate { get; set; }

        [Column(Name = "DateUpdate"), Nullable]
        public DateTime? DateUpdate { get; set; }

        [Column(Name = "DateClouse"), Nullable]
        public DateTime? DateClouse { get; set; }

        [Column(Name = "Name"), NotNull]
        public string Name { get; set; }


        [Column(Name = "IsActive"), NotNull]
        public bool IsActive { get; set; }


        [Column(Name = "Status"), NotNull]
        public short Status { get; set; }

        [Column(Name = "ShortDescription"), NotNull]
        public bool ShortDescription { get; set; }

        [Column(Name = "FullDescription")]
        public string FullDescription { get; set; }
    }
}