﻿using System;
using System.Collections.Generic;

namespace TeamWork.Infrastructure.Model
{
    public class TaskDto
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public ProjectsDto Projects { get; set; }

        public int UserId { get; set; }

        public UserDto User { get; set; }

        public int ReporterUserId { get; set; }

        public UserDto Reporter { get;  set; }


        public DateTime DateCreate { get; set; }

        public DateTime? DateUpdate { get; set; }

        public DateTime? DateClouse { get; set; }

        public string Name { get; set; }

        public bool Status { get; set; }

        public bool ShortDescription { get; set; }

        public string FullDescription { get; set; }

        public IEnumerable<CommentsDto> Comments { get; set; } 

        public IEnumerable<LogWorkDto> LogWork { get; set; } 
    }
}