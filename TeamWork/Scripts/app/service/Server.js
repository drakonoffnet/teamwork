﻿angular.module('teamWork')
  .service('Server', ['$q', '$http', function ($q, $http) {
      var service = this;

    service.api = {
        GetAllUser: ""
    };

    service.httpMetod = function (httpMethod, url, data) {
        var defer = $q.defer();

        var req = {
            method: httpMethod,
            url: url,
            data: data
        };

        $http(req).success(function (request)
        {
            defer.resolve(request);

        }).error(function (request)
        {
            defer.resolve([]);
        });

        return defer.promise;
    };

}]);
