﻿namespace TeamWork.Infrastructure.Database.Entity
{
    public interface IUser
    {
      int Id { get; set; }

      string UserName { get; set; }

      string Email { get; set; }

      bool IsActive { get; set; }
    }
}