﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToDB.Mapping;


namespace TeamWork.DAL.DataObjects {

    [Table(Name = "Users")]
    public class User 
    {
        [Column(Name = "Id")]
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "UserName")]
        public string UserName { get; set; }

        [Column(Name = "Email")]
        public string Email { get; set; }

        [Column(Name = "IsActive")]
        public bool IsActive { get; set; }
    }
}
