﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using LinqToDB;
using TeamWork.DAL.DataObjects;
using TeamWork.Infrastructure.Model;

namespace TeamWork.Core.Service {
    public class RolesService : BaseService, IRolesService
    {
        public RolesService()
        {
            Mapper.CreateMap<Role, RoleDto>();
            Mapper.CreateMap<RoleDto, Role>();
        }

        public List<RoleDto> GetRoles()
        {
            return Mapper.Map<List<RoleDto>>(Context.Roles);
        }

        public RoleDto GetRole(int roleId)
        {
            return Mapper.Map<RoleDto>(Context.Roles.FirstOrDefault(c => c.RoleId == roleId));
        }

        public bool InsertRole(RoleDto item)
        {
            try
            {
                Context.Insert(Mapper.Map<Role>(item));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateRole(RoleDto item)
        {
            try
            {
                Context.Roles
                    .Where(c=>c.RoleId==item.RoleId)
                    .Set(c => c.RoleName, item.RoleName)
                    .Update();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        internal int GetRoleByUser(int userId)
        {
            return Context.UsersInRoles.FirstOrDefault(c => c.UserId == userId).RoleId;
        }

        internal bool InsertRoleToUser(int roleId, int userId)
        {
            try
            {
                Context.UsersInRoles
                    .Value(c => c.RoleId, roleId)
                    .Value(c => c.UserId, userId)
                    .Insert();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal bool UpdateRoleToUser(int userId, int newRoleId)
        {
            try
            {
                Context.UsersInRoles.Where(c => c.UserId == userId).Set(c => c.RoleId, newRoleId);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
