﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamWork.Infrastructure.Model {
    public interface IRolesService
    {
        List<RoleDto> GetRoles();

        RoleDto GetRole(int roleId);

        bool InsertRole(RoleDto item);

        bool UpdateRole(RoleDto item);

    }
}
