﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToDB.Mapping;

namespace TeamWork.DAL.DataObjects
{
    [Table(Name = "webpages_UsersInRoles")]
    public class UsersInRole
    {
        [Column(Name = "UserId")]
        public int UserId { get; set; }

        [Column(Name = "RoleId")]
        public int RoleId { get; set; }
    }
}
