﻿'use strict';
angular.module('teamWork', [
    'ngResource',
    'ngRoute',
    'ngSanitize'
]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider
           .when('/', {
               templateUrl: 'views/main.html',
               controller: 'MainCtrl'
           })
           .when('/setup', {
               templateUrl: 'views/setup.html',
               controller: 'SetupCtrl'
           })
           .when('/about', {
               templateUrl: 'views/about.html',
               controller: 'AboutCtrl'
           })
           .otherwise({
               redirectTo: '/'
           });
}]);
