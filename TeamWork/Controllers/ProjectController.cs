﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TeamWork.Controllers.Base;

namespace TeamWork.Controllers
{
    public class ProjectController : BaseController
    {
        public ActionResult Index()
        {
            var data = ProjectService.GetProjects(10, 0);

            return View(data);
        }

        public ActionResult Edit(int id)
        {
            throw new NotImplementedException();
        }

        public ActionResult Details(int id)
        {
            throw new NotImplementedException();
        }

        public ActionResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
