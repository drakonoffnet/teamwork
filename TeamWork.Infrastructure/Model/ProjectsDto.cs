﻿using System;
using System.Collections.Generic;

namespace TeamWork.Infrastructure.Model
{
    public class ProjectsDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime DateCreate { get; set; }

        public int CountTask { get; set; }

        public List<TaskDto> Tasks { get; set; }

        public bool IsActive { get; set; }
    }
}