﻿using System;
using LinqToDB.Mapping;

namespace TeamWork.DAL.DataObjects
{
   [Table(Name = "Projects")]
    public class Projects
    {
       [Column(Name = "Id")]
       [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "Name"), NotNull]
        public string Name { get; set; }

        [Column(Name = "DateCreate"), NotNull]
        public DateTime DateCreate { get; set; }

        [Column(Name = "IsActive"), NotNull]
        public bool IsActive { get; set; }
    }
}