﻿CREATE TABLE [dbo].[UserProjects](
	[UserId] [int] NOT NULL,
	[ProjectId] [int] NOT NULL,
 CONSTRAINT [PK_UserProjects] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[ProjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY], 
    CONSTRAINT [FK_UserProjects_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([Id]), 
    CONSTRAINT [FK_UserProjects_Projects] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Users]([Id])
) ON [PRIMARY]

GO

CREATE INDEX [UserId] ON [dbo].[UserProjects] ([UserId])
GO
CREATE INDEX [ProjectId] ON [dbo].[UserProjects] ([ProjectId])
GO