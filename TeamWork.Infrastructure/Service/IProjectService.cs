﻿using System.Collections.Generic;
using TeamWork.Infrastructure.Model;

namespace TeamWork.Infrastructure.Service
{
    public interface IProjectService
    {
        IEnumerable<ProjectsDto> GetProjects(int take = 10, int skip = 0);


        ProjectsDto GetProject(int projectId);


        ProjectsDto InsertProject(ProjectsDto item);


        bool UpdateProject(ProjectsDto item);


        bool DeleteProject(int projectId);

    }
}