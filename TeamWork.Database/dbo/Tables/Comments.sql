﻿CREATE TABLE [dbo].[Comments]
(
	[Id] [int] IDENTITY(1,1) NOT NULL, 
    [TaskId] INT NOT NULL, 
    [UserId] INT NOT NULL, 
    [Comment] NCHAR(10) NOT NULL, 
    CONSTRAINT [FK_Comments_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([Id]), 
    CONSTRAINT [FK_Comments_Task] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[Task]([Id]), 
    CONSTRAINT [PK_Comments] PRIMARY KEY ([Id])
)

GO

CREATE INDEX [IX_Comments_UserId] ON [dbo].[Comments] ([UserId], [TaskId])

GO

CREATE INDEX [IX_Comments_TaskId] ON [dbo].[Comments] ([TaskId])
GO