﻿using LinqToDB.Mapping;

namespace TeamWork.DAL.DataObjects
{
    [Table(Name = "LogWork")]
    public class LogWork
    {
        [Column(Name = "Id")]
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "TaskId"), NotNull]
        public int TaskId { get; set; }

        [Column(Name = "UserId"), NotNull]
        public int UserId { get; set; }

        [Column(Name = "Seconds"), NotNull]
        public long Seconds { get; set; }
    }
}