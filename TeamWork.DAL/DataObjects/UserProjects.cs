﻿using LinqToDB.Mapping;

namespace TeamWork.DAL.DataObjects
{
    [Table(Name = "UserProjects")]
    public class UserProjects
    {
        [Column(Name = "UserId"), NotNull]
        public int UserId { get; set; }

        [Column(Name = "ProjectId"), NotNull]
        public int ProjectId { get; set; }
    }
}