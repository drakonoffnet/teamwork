﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using LinqToDB;
using TeamWork.DAL.DataObjects;
using TeamWork.Infrastructure.Model;
using TeamWork.Infrastructure.Service;

namespace TeamWork.Core.Service
{
    public class UsersService : BaseService, IUsers
    {
        private RolesService _rolesService = new RolesService();

        public UsersService()
        {
            Mapper.CreateMap<User, UserDto>();
            Mapper.CreateMap<UserDto, User>();
        }

        public IEnumerable<UserDto> GetUsers(int take = 10, int skip = 0)
        {
            var users = (from user in Context.Users.Skip(skip).Take(take)
                        join usersInRole in Context.UsersInRoles on user.Id equals usersInRole.UserId
                        join role in Context.Roles on usersInRole.RoleId equals role.RoleId
                        select new UserDto
                        {
                            Email = user.Email,
                            Id = user.Id,
                            IsActive = user.IsActive,
                            UserName = user.UserName,
                            RoleId = role.RoleId,
                            RoleName = role.RoleName
                        }).ToList();

            return users;
        }

        public UserDto GetUser(int userId)
        {
            //var user = Context.Users.FirstOrDefault(c => c.Id == userId);

            //var result = Mapper.Map<UserDto>(user);

            //result.RoleId = _rolesService.GetRoleByUser(userId);
            
            var users = (from user in Context.Users.Where(c => c.Id == userId)
                             join usersInRole in Context.UsersInRoles on user.Id equals usersInRole.UserId
                            join role in Context.Roles on usersInRole.RoleId equals role.RoleId
                            select new UserDto
                            {
                                Email = user.Email,
                                Id = user.Id,
                                IsActive = user.IsActive,
                                UserName = user.UserName,
                                RoleId = role.RoleId,
                                RoleName = role.RoleName
                            }).ToList();


            return users.FirstOrDefault();
        }

        public IEnumerable<UserDto> GetActiveUsers()
        {
            return Context.Users.Where(c=>c.IsActive).Select(c => new UserDto()
            {
                Email = c.Email,
                Id = c.Id,
                IsActive = c.IsActive,
                UserName = c.UserName
            }).ToList();
        }

        public IEnumerable<UserDto> FindUserByName(string nameUser)
        {


            return Context.Users.Where(c => c.UserName.Contains(nameUser))
                .Select(c => new UserDto()
                {
                    Email = c.Email,
                    Id = c.Id,
                    IsActive = c.IsActive,
                    UserName = c.UserName,
                    
                }).ToList();
        }

        public IEnumerable<UserDto> FindUserByEmail(string email)
        {
            return Context.Users.Where(c => c.Email.Contains(email)).Select(c => new UserDto()
            {
                Email = c.Email,
                Id = c.Id,
                IsActive = c.IsActive,
                UserName = c.UserName
            }).ToList();
        }

        public UserDto InsertUser(UserDto item)
        {
            try
            {
               var newUser =  Context.GetTable<User>().InsertWithIdentity(() => new User()
                {
                    Email = item.Email,
                    IsActive = item.IsActive,
                    UserName = item.UserName
                });

                item.Id = Convert.ToInt32(newUser);

                _rolesService.InsertRoleToUser(item.RoleId, item.Id);

                return item;
            }
            catch (Exception){

                return item;
            }
        }
        
        public bool UpdateUser(UserDto item)
        {
            try
            {
                Context.Update(Mapper.Map<User>(item));

                _rolesService.UpdateRoleToUser(item.Id, item.RoleId);
                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteUser(int userId)
        {
            try
            {
                Context.Users
                    .Where(p => p.Id == userId)
                    .Set(p => p.IsActive, false)
                    .Update();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}