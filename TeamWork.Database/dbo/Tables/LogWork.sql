﻿CREATE TABLE [dbo].[LogWork]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [TaskId] INT NOT NULL, 
    [UserId] INT NOT NULL, 
    [Seconds] BIGINT NOT NULL, 
    CONSTRAINT [FK_LogWork_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([Id])
)

GO

CREATE INDEX [IX_LogWork_TaskForUser] ON [dbo].[LogWork] ([TaskId], [UserId])
