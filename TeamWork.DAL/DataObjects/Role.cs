﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using LinqToDB.Mapping;

namespace TeamWork.DAL.DataObjects 
{

    [Table(Name = "webpages_Roles")]
    public class Role 
    {
        [Column(Name = "RoleId")]
        [PrimaryKey, Identity]
        public int RoleId { get; set; }

        [Column(Name = "RoleName"), NotNull]
        public string RoleName { get; set; }
    }
}
