﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using LinqToDB;
using TeamWork.DAL.DataObjects;
using TeamWork.Infrastructure.Model;
using TeamWork.Infrastructure.Service;

namespace TeamWork.Core.Service
{
    public class LogWorkService : BaseService, ILogWorkService
    {
        public LogWorkService()
        {
            Mapper.CreateMap<LogWorkDto, LogWork>();

            Mapper.CreateMap<LogWork, LogWorkDto>();
        }

        public LogWorkDto Insert(LogWorkDto item)
        {
            var itemId = Context.GetTable<LogWork>().InsertWithIdentity(() => Mapper.Map<LogWork>(item));

            item.Id = Convert.ToInt32(itemId);
            return item;
        }

        public bool Update(LogWorkDto item)
        {
            try
            {
                Context.Update(Mapper.Map<LogWork>(item));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<LogWorkDto> GetLogWorkTask(int userId, int taskId)
        {
            var data = Context.LogWorks.Where(c => c.UserId == userId && c.TaskId == taskId);

            return Mapper.Map<List<LogWorkDto>>(data);
        } 
    }
}