﻿using LinqToDB.Mapping;

namespace TeamWork.DAL.DataObjects
{
    [Table(Name = "Comments")]
    public class Comments
    {
        [Column(Name = "Id")]
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "TaskId"), NotNull]
        public int TaskId { get; set; }

        [Column(Name = "UserId"), NotNull]
        public int UserId { get; set; }

        [Column(Name = "Comment")]
        public string Comment { get; set; }
    }
}