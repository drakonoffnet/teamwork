﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamWork.Infrastructure.Model 
{
    public class RoleDto 
    {
        public int RoleId { get; set; }

        public string RoleName { get; set; }
    }
}
