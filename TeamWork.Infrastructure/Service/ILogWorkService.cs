﻿using System.Collections.Generic;
using TeamWork.Infrastructure.Model;

namespace TeamWork.Infrastructure.Service
{
    public interface ILogWorkService
    {
        LogWorkDto Insert(LogWorkDto item);

        bool Update(LogWorkDto item);

        List<LogWorkDto> GetLogWorkTask(int userId, int taskId);
    }
}