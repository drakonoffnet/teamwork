﻿namespace TeamWork.Infrastructure.Model
{
    public class LogWorkDto
    {
        public int Id { get; set; }

        public int TaskId { get; set; }

        public int UserId { get; set; }

        public long Seconds { get; set; }
    }
}