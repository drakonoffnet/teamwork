﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamWork.Controllers.Base;
using TeamWork.Infrastructure.Model;

namespace TeamWork.Controllers
{
    public class DirectoryController : BaseController
    {
        //
        // GET: /Directory/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserRole()
        {
            var model = RoleService.GetRoles();

            return View(model);
        }

        public ActionResult AddNewRole(RoleDto item)
        {
            if (TryValidateModel(item))
            {
                RoleService.InsertRole(item);
            }

            return RedirectToAction("UserRole");
        }

        [HttpPost]
        public ActionResult EditRole(int roleId)
        {
            var model = RoleService.GetRole(roleId);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditRole(RoleDto item)
        {
            if (TryValidateModel(item))
            {
                RoleService.UpdateRole(item);
                return RedirectToAction("UserRole");
            }

            return View(item);
        }

     
    }
}
