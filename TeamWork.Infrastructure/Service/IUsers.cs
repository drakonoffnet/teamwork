﻿using System.Collections.Generic;
using TeamWork.Infrastructure.Model;

namespace TeamWork.Infrastructure.Service
{
    public interface IUsers
    {
        IEnumerable<UserDto> GetUsers(int count = 10, int skip = 0);

        UserDto GetUser(int userId);

        IEnumerable<UserDto> GetActiveUsers();

        IEnumerable<UserDto> FindUserByName(string nameUser);

        IEnumerable<UserDto> FindUserByEmail(string email);

        UserDto InsertUser(UserDto item);

        bool UpdateUser(UserDto item);

        bool DeleteUser(int userId);
    }
}