﻿CREATE TABLE [dbo].[Projects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](255) NOT NULL,
	[DateCreate] [datetime] NOT NULL,
	[IsActive] BIT NOT NULL DEFAULT ((0)), 
    [Code] NCHAR(10) NULL, 
    CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE INDEX [IX_DateCreate] ON [dbo].[Projects] ([DateCreate])
