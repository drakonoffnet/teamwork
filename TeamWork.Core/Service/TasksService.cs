﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using LinqToDB;
using TeamWork.DAL.DataObjects;
using TeamWork.Infrastructure.Model;
using TeamWork.Infrastructure.Service;

namespace TeamWork.Core.Service
{
    public class TasksService : BaseService, ITasksService
    {
        public TasksService()
        {
            Mapper.CreateMap<Task, TaskDto>();
            Mapper.CreateMap<List<Task>, List<TaskDto>>();

            Mapper.CreateMap<TaskDto, Task>();
            Mapper.CreateMap<List<TaskDto>, List<Task>>();
        }

        public List<TaskDto> GetTaskForProject(int projectId)
        {
            var result = (from task in Context.Tasks.Where(c => c.ProjectId == projectId)
                          join project in Context.Projects on task.ProjectId equals project.Id
                          join u in Context.Users on task.UserId equals u.Id
                          join reporter in Context.Users on task.UserId equals reporter.Id
                          select new TaskDto()
                          {
                              Id = task.Id,
                              Projects = new ProjectsDto()
                              {
                                  Id = project.Id,
                                  IsActive = project.IsActive,
                                  Name = project.Name,
                                  DateCreate = project.DateCreate
                              },
                              DateCreate = task.DateCreate,
                              Name = task.Name,
                              ProjectId = task.ProjectId,
                              User = new UserDto()
                              {
                                  Id = u.Id,
                                  IsActive = u.IsActive,
                                  Email = u.Email,
                                  UserName = u.UserName
                              },
                              Reporter = new UserDto()
                              {
                                  Id = reporter.Id,
                                  IsActive = reporter.IsActive,
                                  Email = reporter.Email,
                                  UserName = reporter.UserName
                              },
                              Comments = new List<CommentsDto>(),
                              DateClouse = task.DateClouse,
                              DateUpdate = task.DateUpdate,
                              ShortDescription = task.ShortDescription,
                              Status = task.IsActive
                          }).ToList();

            return result;
        }

        public List<TaskDto> GetAllTaskOneUser(int userId)
        {
            var result = (from task in Context.Tasks.Where(c => c.UserId == userId)
                          join project in Context.Projects on task.ProjectId equals project.Id
                          join u in Context.Users on task.UserId equals u.Id
                          join reporter in Context.Users on task.UserId equals reporter.Id
                          select new TaskDto()
                {
                    Id = task.Id,
                    Projects = new ProjectsDto()
                    {
                        Id = project.Id,
                        IsActive = project.IsActive,
                        Name = project.Name,
                        DateCreate = project.DateCreate
                    },
                    DateCreate = task.DateCreate,
                    Name = task.Name,
                    ProjectId = task.ProjectId,
                    User = new UserDto()
                    {
                        Id = u.Id,
                        IsActive = u.IsActive,
                        Email = u.Email,
                        UserName = u.UserName
                    },
                    Reporter = new UserDto()
                    {
                        Id = reporter.Id,
                        IsActive = reporter.IsActive,
                        Email = reporter.Email,
                        UserName = reporter.UserName
                    },
                    Comments = new List<CommentsDto>(),
                    DateClouse = task.DateClouse,
                    DateUpdate = task.DateUpdate,
                    ShortDescription = task.ShortDescription,
                    Status = task.IsActive
                }).ToList();

            return result;
        }

        public TaskDto InserTask(TaskDto item)
        {
            item.Id = Convert.ToInt32(Context.InsertWithIdentity(Mapper.Map<Task>(item)));

            return item;
        }

        public bool СhangeAssignee(int taskId, int newUserId)
        {
            try
            {
                Context.Tasks
                    .Where(p => p.Id == taskId)
                    .Set(p => p.UserId, newUserId)
                    .Update();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateTask(TaskDto item)
        {
            try
            {
                Context.Update(Mapper.Map<Task>(item));

                return true;    
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteTask(int taskId)
        {
            try
            {
                Context.Tasks
                    .Where(p => p.Id == taskId)
                    .Set(p => p.IsActive, false)
                    .Update();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
