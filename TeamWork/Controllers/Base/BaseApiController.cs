﻿using System.Web.Http;
using TeamWork.Core.Service;
using TeamWork.Infrastructure.Service;

namespace TeamWork.Controllers.Base
{
    public class BaseApiController : ApiController
    {
        protected IProjectService _ProjectService = new ProjectService();

        protected IUsers _Users = new UsersService();
    }
}